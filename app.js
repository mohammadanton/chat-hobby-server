const express = require('express');
const app = express();
const consola = require('consola')
const http = require('http').Server(app);
const io = require('socket.io')(http);
const shortid = require('shortid');
const ROOM = require('./room.js');
const PrivateRoom = require('./private_room.js');
// Import from Nguyen
const auth = require('./auth').auth;
const User = require('./user.js').User;
const Token = require('./user.js').Token;
// graphql client
// import { request, GraphQLClient } from 'graphql-request';
const { request, GraphQLClient } = require('graphql-request');

let user = [];

var endpoint = "https://candied-rooster.glitch.me/graphql";

consola.info("Init");

const client = new GraphQLClient(endpoint, { headers: {} });

const query = `{
       everyUser {
	 id
	 name
       }
}`;

const queryPrivateRoom = `{
       everyPrivateroom {
       roomId
       name
       client
       clientId
       }
}`;

const queryRoom = `{
       everyRoom {
       id
       creator
       name
       client
       }
}`;


client.request(query).then(data => {
	//console.log(data['everyUser']);

	consola.info("Init: "+data['everyUser'].length+" user");
	for (var i = 0; i < data['everyUser'].length; i++) {
		var newUser = new User(data['everyUser'][i]['id'], data['everyUser'][i]['name'], data['everyUser'][i]['name']);
		user.push(newUser);
		//console.log(JSON.stringify(newUser));
	}

	initRoom();
	initPrivateRoom();
});

var globPrivateRoom;

function initRoom() {

	client.request(queryRoom).then(data => {
		//console.log(data['everyPrivateroom']);
		var dt = data['everyRoom'];
		//globPrivateRoom = dt;
		consola.info("Init: "+dt.length+"private room");
		for (var i = 0; i < dt.length; i++) {
			console.log(dt[i]['name']);
			//let newPrivateRoom = new PrivateRoom(senderId, receiverId, senderName, receiverName);
			//let newRoomId = createPrivateRoom(dt[i]['clientId'][0], dt[i]['clientId'][1], dt[i]['client'][0], dt[i]['client'][1]);

			let roomId = dt[i]['id'];
			let roomName = dt[i]['name'];
			let clientId = dt[i]['creator'];

			let newRoom = new ROOM(roomId, roomName, clientId);
			allRoomObj[roomId] = newRoom;
			roomList[roomId] = roomName;


			for (var j= 0; j < dt[i]['client'].length; j++) {
				//consola.error(JSON.stringify(dt[i]['client']));

				var client = user.find(ele => ele.id === dt[i]['client'][j]);


				if (client.id != allRoomObj[roomId].creator) {

					allRoomObj[roomId].addClient(client.id);

					client.room.push(roomId);
				} else {
					client.room.push(roomId);
				}
				//	room.addClient(clientID);
				//
				const main = io.on('connection', (socket) => {
					socket.join(roomId);
				});


			}

			consola.success("Init: room "+JSON.stringify(allRoomObj[roomId]));
		}
	});


}


function initPrivateRoom() {

	client.request(queryPrivateRoom).then(data => {
		//console.log(data['everyPrivateroom']);
		var dt = data['everyPrivateroom'];
		globPrivateRoom = dt;
		consola.info("Init: "+dt.length+"private room");
		for (var i = 0; i < dt.length; i++) {
			console.log(dt[i]['client']);
			//let newPrivateRoom = new PrivateRoom(senderId, receiverId, senderName, receiverName);
			let newRoomId = createPrivateRoom(dt[i]['clientId'][0], dt[i]['clientId'][1], dt[i]['client'][0], dt[i]['client'][1]);
			//	    let newRoomId = dt[i]['roomId'];
			//	    let newRoomName = dt[i]['name'];
			//let newRoomName = allPrivateRoom[newRoomId].name;

			// get sender and receiver data in user array
			let sender = user.find(ele => ele.id === dt[i]['clientId'][0]);
			let receiver = user.find(ele => ele.id === dt[i]['clientId'][1]);

			// get the obj contains private chat rooms of the sender

			// update friend list of both users
			// sender to receiver id
			// reciever to sender id
			//console.log("ROOOOM"+newRoomId);
			sender.friend[dt[i]['clientId'][1]] = newRoomId;
			receiver.friend[dt[i]['clientId'][0]] = newRoomId;

			//console.log("sender "+JSON.stringify(sender));
			//console.log("rec "+JSON.stringify(receiver));


			//console.log("new pr "+newPrivateRoom);
			//        var newUser = new User(dt[i]['id'], dt[i]['name'], dt[i]['name']);
			//       user.push(newUser);
		}
	});


}

//	.catch(err => {
//         console.log(err.response); // GraphQL response errors
//         console.log(err.response); // Response data if available
//     });



let token = [];

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/views/login.html');
});
//==========================================
// Routing: 
app.get('/chat', (req, res) => {
	res.sendFile(__dirname + '/views/index.html');
});

const allRoomObj = {};
const roomList = {};
const allPrivateRoom = {};
//==============================================
// ROOM FUNCTIONS
//==============================================
/**
 * Create a room function
 * @param {*} roomName - received from client-side
 * @param {*} clientID - received from client-side
 */
// roomID is not yet generated randomly to run tests
const createRoom = (clientId, roomName) => {
	// check if both params are passed into
	if (typeof clientId === 'undefined' || typeof roomName === 'undefined') {
		throw new Error('Error: params are not passed into the function');
	} else {


		const query = `mutation addRoom($creator: String!, $name: String!, $client: [String!]!) {
  addRoom(creator: $creator, name: $name, client: $client) {
  id
  creator
  name
  client
  quantity
    }
}`;

		// TODO
		var client = [clientId];


		const variables = {
			creator: clientId,
			name: roomName,
			client: client,
		};

		request(endpoint, query, variables).then( function(data) {

			let roomId = data['addRoom']['id'];


			let newRoom = new ROOM(roomId, roomName, clientId);
			allRoomObj[roomId] = newRoom;
			roomList[roomId] = roomName;
			consola.success("Room created "+JSON.stringify(roomList[roomId]));
			return roomId;
		});



	}
}

/**
 * Delete a room function
 * @param {*} clientID 
 * @param {*} roomID 
 */
const deleteRoom = (clientID, roomID) => {
	if (typeof clientID === 'undefined' || typeof roomID === 'undefined') {
		throw new Error('Error: params are not passed into the function');
	} else if (allRoomObj[roomID] === undefined) {
		throw new Error('Error: Room does not exist !!');
	} else {
		// only allows the room's creator
		let roomCreator = allRoomObj[roomID].creator;
		if (clientID !== roomCreator) {
			throw new Error("Error: Only the room's creator is allowed to delete it !!");
		} else {
			delete allRoomObj[roomID];
			delete roomList[roomID];
		}
	}
}

/**
 * Clients join rooms function -> add clients to room
 * @param {*} clientID 
 * @param {*} roomID 
 */
const joinRoom = (clientID, roomID) => {
	if (typeof clientID === 'undefined' || typeof roomID === 'undefined') {
		throw new Error('Error: params are not passed into the function');
	} else if (allRoomObj[roomID] === undefined) {
		throw new Error('Error: Room does not exist !!');
	} else {
		let room = allRoomObj[roomID];
		room.addClient(clientID);

		const query = `mutation updateRoom($id: String!, $creator: String!, $name: String!, $client: [String!]!, $quantity: Int!) {
  updateRoom(id: $id, creator: $creator, name: $name, client: $client, quantity: $quantity) {
  id
  creator
  name
  client
  quantity
    }
}`;

		// TODO
		var creator = room.creator;
		var roomName = room.name;
		var client = room.client;
		var quantity = room.quantity + 1;

		const variables = {
			id: roomID,
			creator: creator,
			name: roomName,
			client: client,
			quantity: quantity,
		};

		request(endpoint, query, variables).then( function(data) {

			//consola.error(clientID+" success join room  "+ roomName);

		});

	}
}

/**
 * Clients leave rooms function -> remove clients from room
 * @param {*} clientID 
 * @param {*} roomID
 */
const leaveRoom = (clientID, roomID) => {
	if (typeof clientID === 'undefined' || typeof roomID === 'undefined') {
		throw new Error('Error: params are not passed into the function');
	} else if (allRoomObj[roomID] === undefined) {
		throw new Error('Error: Room does not exist !!');
	} else {
		let room = allRoomObj[roomID];
		room.removeClient(clientID);
	}
}

/**
 * Clients change the room's name -> only allow the room's creator to change name
 * @param {*} clientID 
 * @param {*} roomID 
 * @param {*} newRoomName 
 */

const changeRoomName = (clientID, roomID, newRoomName) => {
	if (typeof clientID === 'undefined' || typeof roomID === 'undefined' || typeof newRoomName === 'undefined') {
		throw new Error('Error: params are not passed into the function');
	} else if (allRoomObj[roomID] === undefined) {
		throw new Error('Error: Room does not exist !!');
	} else {
		let room = allRoomObj[roomID];
		let roomCreator = allRoomObj[roomID].creator;
		if (clientID !== roomCreator) {
			throw new Error("Error: Only the room's creator is allowed to change room name !!");
		} else {
			room.changeName(newRoomName);
			roomList[roomID] = newRoomName;
		}
	}
}

//=============================================
// PRIVATE ROOM FUNCTIONS
//=============================================
//
//


const createPrivateRoom = (senderId, receiverId, senderName, receiverName) => {
	// check if both params are passed into
	if (typeof senderId === 'undefined' || typeof receiverId === 'undefined' || typeof senderName === 'undefined' || typeof receiverName === 'undefined') {
		throw new Error('Error: params are not passed into the function');
	} else {
		let newPrivateRoom = new PrivateRoom(senderId, receiverId, senderName, receiverName);
		newPrivateRoom.addClient(receiverId);
		let roomId = newPrivateRoom.id;
		allPrivateRoom[roomId] = newPrivateRoom;

		//console.log("success create private room "+room);
		consola.success("private room created"+roomId);



		return roomId;
	}



}


//==============================================
// SOCKET.IO EVENTS
//==============================================
const main = io.on('connection', (socket) => {

	// receive clientId when an user logins
	socket.on('send clientId', (id) => {
		// new code to fix room msg events
		// default join 'main' room
		socket.join('main')
		//============================
		let clientId = id;
		// find the client info with clientId
		let connectClient = user.find(ele => ele.id === clientId);
		consola.success("client connected: "+JSON.stringify(connectClient));
		// store the current socket.id in the user obj to be used for private msg
		connectClient.socketId = socket.id;
		socket.username = connectClient.username;
		io.to(socket.id).emit('reconnect', connectClient, roomList);
		io.sockets.emit('user connect',{
			"connectClient": connectClient, "roomList": roomList, "user": user
		});
		io.sockets.emit('user connect', connectClient, roomList, user);
		//	    console.log(JSON.stringify(connectClient));
		//	    console.log("====");
		//	    console.log(JSON.stringify(roomList));
		//	    console.log("====");
		//	    console.log(JSON.stringify(user));
	});
	//=================================
	// socket.on('disconnect', () => {
	//     // 
	//     console.log(user);
	//     let disconnectClient = user.find(ele => ele.socketId === socket.id);
	//     socket.broadcast.emit('user disconnect', disconnectClient.username);
	//     console.log(socket.username + ' disconnected');
	//     console.log(user);
	// });

	//===================================
	// IMPORT FROM Nguyen
	auth(socket, user, token);
	//===================================
	// Handle private msg events
	//===================================
	socket.on('send private', (senderId, receiverId) => {

		// get sender and receiver data in user array
		let sender = user.find(ele => ele.id === senderId);
		let receiver = user.find(ele => ele.id === receiverId);

		// get the obj contains private chat rooms of the sender
		let senderFriend = sender.friend;
		// check if a room is already created between these two users 
		if (senderFriend.hasOwnProperty(receiverId)) {
			let roomId = senderFriend[receiverId];
			let roomName = allPrivateRoom[roomId].name;
			// send back the roomId, roomName, senderName, receiverName to sender -> create chat room in front send
			// socket.emit('create private chat', roomId, roomName, sender.username, receiver.username);
			socket.emit('create private chat', {
				"roomId": roomId, "roomName": roomName, "senderUsername": sender.username, "senderReceiver": receiver.username
			});
			console.log("==========================room exist, continue chat");
			consola.info("roomid "+roomId);
		} else {
			// create a new private room
			let newRoomId = createPrivateRoom(senderId, receiverId, sender.username, receiver.username);
			//TODO mutation new room 
			console.log("========================todo new room created in graphql");
			console.log('newRoomId: ' + newRoomId);
			let newRoomName = allPrivateRoom[newRoomId].name;
			// update friend list of both users
			sender.friend[receiverId] = newRoomId;
			receiver.friend[senderId] = newRoomId;

			const query = `mutation addPrivateroom($roomId: String!, $name: String!, $client: [String!]!, $clientId: [String!]!) {
  addPrivateroom(roomId: $roomId, name: $name, client: $client, clientId: $clientId) {
  id
  roomId
  name
  client
  quantity
    }
}`;

			// TODO
			var client = [sender.username, receiver.username];
			var clientId = [senderId, receiverId];

			consola.info("roomid "+newRoomId);

			const variables = {
				roomId: newRoomId,
				name: newRoomName,
				client: client,
				clientId: clientId,
			};

			request(endpoint, query, variables).then(data => console.log(data));

			// send back the roomId, roomName, senderName, receiverName to sender -> create chat room in front send
			// socket.emit('create private chat', newRoomId, newRoomName, sender.username, receiver.username);
			socket.emit('create private chat', {
				"roomId": newRoomId, "roomName": newRoomName, "senderUsername": sender.username, "senderReceiver": receiver.username
			});
		}
	})
	// when an user wants to send a private msg -> find receiver -> emit msg and create a chat box in receiver window
	socket.on('private message', (data) => {
		// find sender in user array by socketId
		let sender = user.find(ele => ele.socketId === socket.id);
		// get senderId
		let senderId = sender.id;
		// from roomId -> get receiverId
		let receiverId = data.roomId.split('--').find(ele => ele !== senderId);
		// get socket.id of receiver to emit msg 
		let receiverSocketId = user.find(ele => ele.id === receiverId).socketId;
		let roomName = allPrivateRoom[data.roomId].name;
		// only emit msg to the receiver via receiverSocketId
		socket.broadcast.to(receiverSocketId).emit('private message', {
			roomId: data.roomId,
			roomName: roomName,
			username: sender.username,
			message: data.message
		})
	})

	//======================================================
	// Handle public room events: join, leave, create, delete
	//=======================================================
	socket.on('create room', (roomName, clientId) => {
		try {
			// have user info -> use clientId not socket.id
			let newRoomId = createRoom(clientId, roomName);
			let client = user.find(ele => ele.id === clientId);
			client.room.push(newRoomId);
			// emit a msg back to the sender
			socket.emit('new room', {
				clientName: socket.username,
				newRoomId: newRoomId,
				newRoomName: roomList[newRoomId]
			});
			io.sockets.emit('update room', roomList);
			// new code to fix room msg events -> join newly created room
			socket.join(newRoomId);
			console.log(socket.room);
			console.log(io.sockets.adapter.rooms[newRoomId]);
			//============================
			//
			//


		} catch (err) {
			console.log(err);
			socket.emit('create room error', socket.username, err);
		}
	});

	socket.on('join room', (clientId, roomId) => {
		// check if the client is already in the room
		let alreadyInRoom = allRoomObj[roomId].client.some(ele => ele === clientId);
		if (!alreadyInRoom) {
			joinRoom(clientId, roomId);
			let client = user.find(ele => ele.id === clientId);
			client.room.push(roomId);
			// new code to fix room msg events -> join room
			// console.log(io.sockets.adapter.rooms[roomId]);
			socket.join(roomId);
			// console.log(io.sockets.adapter.rooms[roomId]);
			//============================
			// emit a msg back to the sender
			socket.emit('join room', {
				clientName: socket.username,
				roomId: roomId,
				roomName: roomList[roomId]
			});
		} else {
			socket.emit('join room error', "You are already in the room!");
		}
	});

	socket.on('leave room', (clientId, roomId) => {
		let alreadyInRoom = allRoomObj[roomId].client.some(ele => ele === clientId);
		if (alreadyInRoom) {
			leaveRoom(clientId, roomId);
			let client = user.find(ele => ele.id === clientId);
			let roomIndex = client.room.indexOf(roomId);
			client.room.splice(roomIndex, 1);
			// new code to fix room msg events -> leave room
			// console.log(io.sockets.adapter.rooms[roomId]);;
			socket.leave(roomId)
			// console.log(io.sockets.adapter.rooms[roomId]);;
			//============================
			// emit a msg back to the sender
			socket.emit('leave room', {
				clientName: socket.username,
				roomId: roomId,
				roomName: roomList[roomId]
			});
		} else {
			socket.emit('leave room error', "You are not in the room!");
		}

	});

	socket.on('delete room', (clientId, roomId) => {
		let roomName = roomList[roomId];
		try {
			deleteRoom(clientId, roomId);
			let client = user.find(ele => ele.id === clientId);
			let roomIndex = client.room.indexOf(roomId);
			client.room.splice(roomIndex, 1);
			// new code to fix room msg events -> delete room
			// console.log(io.sockets.adapter.rooms[roomId]);
			// remove all clients from the room
			// get array of all clients in 'roomId'
			io.in(roomId).clients(function (error, clients) {
				if (clients.length > 0) {
					console.log('clients in the room: ');
					console.log(clients);
					clients.forEach(function (socket_id) {
						// clients leave room
						io.sockets.sockets[socket_id].leave(roomId);
					});
				}
			});
			// console.log(io.sockets.adapter.rooms[roomId]);
			//============================
			// emit to all clients new roomList -> update room list
			io.sockets.emit('delete room', roomId, roomName, roomList);
		} catch (err) {
			console.log(err);
			// emit a msg back to the sender
			socket.emit('delete room error', err.message);
		}
	})

	//================================================
	// Handle chat msg events -> broadcast to the room
	//================================================
	socket.on('chat message', (data) => {
		// emit msg to clients in the room only
		socket.broadcast.to(data.roomId).emit('chat message', {
			roomId: data.roomId,
			username: socket.username,
			message: data.message
		});
	});
})


//===========================================
// LISTEN ON PORT 3000
//===========================================
http.listen(3000, () => {
	console.log('listening on *:3000');
});
